try:
    fin = open('input.txt', 'r')
except:
    print('ERROR: input.txt not found')
    exit(1)

print('START PROGRAM...')

#PASS 1 START------------------------------------------------------
ESTAB = {}
progaddr = int('4000', 16)
csaddr = progaddr
totallen = 0
while True:
    line = fin.readline()
    if not line:
        break
    while line[0] != 'H':
        line = fin.readline()
    cslth = int(line[-7:-1], 16)
    totallen += cslth
    if line[1:7].strip() in ESTAB:
        print('ERROR: duplicate external symbol')
        exit(1)
    else:
        ESTAB[line[1:7].strip()] = csaddr
    while line[0] != 'E':
        line = fin.readline()
        if line[0] == 'D':
            length = len(line)-2
            for i in range(1, length, 12):
                if line[i:i+6].strip() in ESTAB:
                    print('ERROR: duplicate external symbol')
                    exit(1)
                else:
                    ESTAB[line[i:i+6].strip()] = int(line[i+6:i+6+6], 16) + csaddr
    csaddr += cslth
fin.close()

#PASS 2 START------------------------------------------------------
fin2 = open('input.txt', 'r')
csaddr = progaddr
execaddr = progaddr
t = 0
alllist = []
while True:
    line = fin2.readline()
    if not line:
        break
    while line[0] != 'H':
        line = fin2.readline()
    cslth = int(line[-7:-1], 16)
    while line[0] != 'E':
        line = fin2.readline()
        if line[0] == 'E':
            t += len(alllist) - t
            break
        if line[0] == 'T':
            onelist = []
            onelist.append(int(line[1:7], 16))
            onelist.append(int(line[7:9], 16))
            onelist.append(line[:-1])
            alllist.append(onelist)
        elif line[0] == 'M':
            if line[10:-1] in ESTAB:
                for i in range(t, len(alllist)):
                    if int(line[1:7], 16) < alllist[i][0]:
                        tModify = i-1
                        break
                    tModify = i
                sideModify = (int(line[1:7], 16)-int(alllist[tModify][2][1:7], 16)) * 2
                sideModify += 9
                numModify = int(alllist[tModify][2][sideModify:sideModify+6], 16)
                if numModify > 0x7FFFFF:
                    numModify = int(alllist[tModify][2][sideModify:sideModify+6], 16) - 0x1000000
                if line[9] == '+':
                    numModify += ESTAB[line[10:-1]]
                elif line[9] == '-':
                    numModify -= ESTAB[line[10:-1]]
                if numModify < 0:
                    numModify = 0xFFFFFF - ~numModify
                alllist[tModify][2] = alllist[tModify][2][0:sideModify] + hex(numModify)[2:].upper().zfill(6) + alllist[tModify][2][sideModify+6:]
            else:
                print('ERROR: undefined external symbol')
fin2.close()

#START OUTPUT FILE------------------------------------------------------
fackaddr = 0
outlist = []
totallen *= 2
for i in range(0, totallen):
    outlist.append('.')
for i in range(0, len(alllist)):
    k = 9
    for j in range(fackaddr+alllist[i][0]*2, fackaddr + (alllist[i][0]*2)+(alllist[i][1]*2)):
        outlist[j] = alllist[i][2][k]
        k += 1
    if i%2==1:
        fackaddr = j+1

row = int(totallen/32)
if row % 32 > 0:
    row += 1
fout = open('output.txt', 'w')
curnum = 0
for i in range(0, row):
    fout.write(hex(progaddr+i*16)[2:].upper())
    fout.write('  ')
    for j in range(curnum, curnum+32):
        if j % 8 == 0:
            fout.write(' ')
        if j >= totallen:
            break
        fout.write(outlist[j])
    fout.write('\n')
    curnum += 32

print('Linking and Loading SUCCESS!')
